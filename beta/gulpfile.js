const gulp = require('gulp');
const handlebars = require('gulp-compile-handlebars');
const rename = require('gulp-rename');
const less = require('gulp-less');
const uglify = require("gulp-uglify");
const minifyCSS = require('gulp-csso');
const pump = require('pump');

function swallowError (error) {
  console.log(error.toString())
  this.emit('end')
}

function requireJsons(fileNames) {
  return fileNames.reduce(function(jsons, fileName, i) {
    jsons[fileName] = require('./data/' + fileNames[i] + '.json');
    return jsons;
  }, {});
}

var data = requireJsons(['references']);

gulp.task('html', () => {
  return gulp.src('./pages/**/*.hbs')
    .pipe(handlebars(data, {
      ignorePartials: true,
      batch: ['./partials', ]
    }))
    .on('error', swallowError)
    .pipe(rename({
      extname: '.html'
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('images', function(){
  return gulp.src("images_prod/**/*")
    .pipe(gulp.dest('dist/images'))
});

gulp.task('scripts', function(){
  return gulp.src("scripts/*")
    .pipe(gulp.dest('dist/scripts'))
});

gulp.task('vendor-js', function(cb){
  pump([
    gulp.src("vendor/**/*.js"),
    uglify(),
    gulp.dest('dist/vendor')
  ], cb);
});

gulp.task('vendor-css', function(cb){
  pump([
    gulp.src(["vendor/**/*.css", "vendor/**/*.less"]),
    less(),
    minifyCSS(),
    gulp.dest('dist/vendor')
  ], cb);
});

gulp.task('vendor-other', function(cb){
  pump([
    gulp.src(['vendor/**/*.png', 'vendor/**/*.gif', 'vendor/**/*.jpg', 'vendor/**/*.eot', 'vendor/**/*.svg', 'vendor/**/*.ttf', 'vendor/**/*.woff']),
    gulp.dest('dist/vendor')
  ], cb);
});

gulp.task('js', function(cb){
  pump([
    gulp.src("js/**/*"),
    uglify(),
    gulp.dest('dist/js')
  ], cb);
});

gulp.task('fonts', function(){
  return gulp.src("fonts/**/*")
    .pipe(gulp.dest('dist/fonts'))
});

gulp.task('files', function(){
  return gulp.src("files/**/*")
    .pipe(gulp.dest('dist/files'))
});

gulp.task('css', function(){
  return gulp.src('css/*.css')
    .on('error', swallowError)
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist/css'))
});

gulp.task('watch', function() {
  gulp.watch('css/*.css', ['css']);
  gulp.watch('scripts/*', ['scripts']);
  gulp.watch('data/*.json', ['html']);
  gulp.watch('js/**/*', ['js']);
  gulp.watch('files/**/*', ['files']);
  gulp.watch('images/**/*', ['images']);
  gulp.watch('images_prod/**/*', ['images']);
  gulp.watch(['pages/**/*.hbs', 'partials/**/*.hbs'], ['html']);
});

gulp.task('compile', [ 'html', 'images', 'vendor-js', 'vendor-css', 'vendor-other', 'css', 'js', 'fonts', 'files', 'scripts']);
